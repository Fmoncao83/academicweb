package br.com.fmoncao.academicweb.repositorio;

import br.com.fmoncao.academicweb.modelo.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlunoRepositorio extends JpaRepository<Aluno, Long> {
}
