package br.com.fmoncao.academicweb.controller;

import br.com.fmoncao.academicweb.modelo.Aluno;
import br.com.fmoncao.academicweb.repositorio.AlunoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/alunos")
public class AlunoController {

    @Autowired
    private AlunoRepositorio alunoRepositorio;

    //Listar
    @GetMapping
    public List<Aluno> listar(){
        return alunoRepositorio.findAll();
    }

    //Incluir
    @PostMapping
    public void incluir(@RequestBody Aluno aluno){
        alunoRepositorio.save(aluno);
    }

    //Alterar
    @PutMapping
    public void alterar(@RequestBody Aluno aluno){
        //Valida campos -- Regras
        alunoRepositorio.save(aluno);
    }

    //Deletar
    @DeleteMapping("/{matricula}")
    public void deletar(@PathVariable Long matricula){
        alunoRepositorio.deleteById(matricula);
    }

    //Listar
    @GetMapping("/{matricula}")
    public Optional<Aluno> ler(@PathVariable Long matricula){
        return alunoRepositorio.findById(matricula);
    }
}
