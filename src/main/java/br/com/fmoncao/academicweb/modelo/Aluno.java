package br.com.fmoncao.academicweb.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Data //Cria os gets, sets, to_String,...
@Entity
public class Aluno {

    @Id
    private Long matricula;
    private String nome;
    private String telefone;
    private String email;
    private Instant dataCadastro;

}
