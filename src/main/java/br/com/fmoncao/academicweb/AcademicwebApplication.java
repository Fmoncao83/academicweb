package br.com.fmoncao.academicweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademicwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademicwebApplication.class, args);
	}

}
